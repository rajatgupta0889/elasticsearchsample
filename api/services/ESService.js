var elasticsearch = require('elasticsearch');
var host = process.env.hotify_es_url;

var client = new elasticsearch.Client({
  host: host
});


exports.index = function(doc, options, cb){
	sails.log.debug('indexing doc ', doc, 'with options', options);
	client.index({
	  index: options && options.index ? options.index : 'hotify',
	  type : options && options.type ? options.type : 'user',
	  body: doc,
	  id: options.id
	}, function (err, resp) {
	  	if(err){
	  		cb(err);
	  	}else {
		  	cb(null, resp);	  		
	  	}
	});
};

exports.update = function(data, options, cb){
    // sails.log.debug('data', data, 'options', options);
    var body = {
        doc: data
    };
    client.update({
        index: options && options.index ? options.index : 'hotify',
        type : options && options.type ? options.type : 'user',
        id: options.id,
        body: body,
    }, function(err, resp){
        if(err){
            console.log("ES Error", err);
            cb(err);
        }else {
            // resp = _.pluck(resp.hits.hits, "_source");
            // console.log('Es Resp',resp);
            cb(null, resp);
        }        
    })            
};

exports.search = function(options, query, override, cb) {
	var body = override ? query : { query: query };

	// console.log('ES: search query ', body);
	client.search({
		index: options && options.index ? options.index : 'hotify',
		type : options && options.type ? options.type : 'user',
		body: body,
		size: options && options.size ? options.size : 10
	}, function(err, resp){
		if(err){
			cb(err);
		}else {
			resp = _.pluck(resp.hits.hits, "_source");
			cb(null, resp);
		}
	});
};
