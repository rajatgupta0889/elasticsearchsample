/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	id: {
            type: 'string',
            primaryKey: true,
        },
    fName:{
    	type: 'string'
    },

    lName:{
    	type: 'string'
    },
    age:{
    	type: 'string'
    },
    designation:{
    	type: 'string'
    }

  },
     add: function(user, cb){

        // sails.log.debug('points models', user_id, event);
        User.create(user,function(err,newUser){
            if(!err){
                console.log("User created ",newUser);
                cb(null,newUser);
            }
            else
                cb(err);

        });
    
    },

    afterCreate: function(user,cb){
        var doc = user;
        sails.log("User  :",doc);
        ESService.index(doc, {id: doc.id,type:"user"}, function(err, indexDoc){
            if(err){
                sails.log.error('ES: User index error', err);
            }else {
                sails.log.debug('ES: User indexed.',indexDoc);
            }
        });
        cb();

    },
    search: function(cb){
        var options = {};
    
        var query = {
            "query": {
                "match_all": {}
            }
        }
        ESService.search(options,query,true,function(err,users){
            if(!err)
                cb(null,users);
            else{
                cb(err);
            }
        })
    }

};

